(function($){
  //clear the search field on page load (we don't want these values preserved when a user hits refresh)
  $('#site-search').val('');

  //stop search button from submitting when users click on the button when the search field is hidden, or when the field is blank
  $('.site-search button').click(function() {
    $searchbox = $('.site-search input[type="search"]');
    if ($searchbox.width() == 0) { 
      $searchbox.focus();   
      return false;
    } else if ($searchbox.val() == '') {
      return false;
    }
  });
})(jQuery);